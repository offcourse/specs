(defproject offcourse/specs "0.1.24-SNAPSHOT"
  :description     "This library contains the data specifications (clojure.spec) for offcourse projects"
  :url             "https://gitlab.com/offcourse/specs"
  :license         {:name "MIT License"
                    :url "http://opensource.org/licenses/MIT"}
  :plugins         [[lein-cljsbuild "1.1.7"]
                    [lein-pprint "1.1.2"]]
  :dependencies    [[org.clojure/clojure         "1.9.0-alpha17"]
                    [org.clojure/clojurescript   "1.9.854"]]
  :repositories [["snapshots" {:url "https://clojars.org/repo"
                               :username :env/CLOJARS_USER
                               :password :env/CLOJARS_PASS}]
                  ["releases" {:url "https://clojars.org/repo"
                              :sign-releases false
                              :username :env/CLOJARS_USER
                              :password :env/CLOJARS_PASS}]]
  :profiles {:dev {:dependencies [[com.cemerick/piggieback "0.2.2"]
                                  [org.clojure/tools.nrepl "0.2.10"]]
                   :repl-options {:nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}}}
  :deploy-repositories [["clojars" {:url "https://clojars.org/repo"
                                    :username :env/CLOJARS_USER
                                    :password :env/CLOJARS_PASS
                                    :sign-releases false}]]
  :aliases {"bump" ["change" "version" "leiningen.release/bump-version"]}
  :release-tasks [["change" "version" "leiningen.release/bump-version" "release"]
                  ["deploy" "clojars"]]
  :cljsbuild       {:builds [{:id "prod"
                              :source-paths ["src"]
                              :compiler {:target :nodejs
                                         :optimizations :simple
                                         :pretty-print true
                                         :parallel-build true}}]})
