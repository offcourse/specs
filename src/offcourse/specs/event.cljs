(ns offcourse.specs.event
  (:require [cljs.spec.alpha :as spec]))

(defmulti  event-spec (fn [[event-type _ :as event]] event-type))

(spec/def :event/valid (spec/multi-spec event-spec ::event-type))

(spec/def :event/types #{:triggered :received :found :not-found :removed :rejected
                         :initialized :transformed :failed :compressed :saved
                         :forked :rendered
                         :refreshed :requested :signed-in :signed-up})
