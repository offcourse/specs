(ns offcourse.specs.base
  (:require [cljs.spec.alpha :as spec]
            [offcourse.specs.helpers :refer [min-length]]))

(def url-regex #"^https?://.+\..+")
(spec/def :base/flags (spec/* string?))
(spec/def :base/tags (spec/* string?))
(spec/def :base/url (spec/and string? #(re-matches url-regex %)))
(spec/def :base/slug string?)
(spec/def :base/organization string?)
(spec/def :base/user-name (spec/and string? #(min-length % 3)))
(spec/def :base/timestamp int?)
