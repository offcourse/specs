(ns offcourse.specs.portrait
  (:require [cljs.spec.alpha :as spec]))

(spec/def ::portrait-url string?)
(spec/def ::portrait-data any?)

(spec/def :portrait/valid (spec/keys :req-un [:base/user-name ::portrait-url]
                                     :opt-un [::portrait-data]))
