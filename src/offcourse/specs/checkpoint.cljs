(ns offcourse.specs.checkpoint
  (:require [cljs.spec.alpha :as spec]
            [offcourse.specs.helpers :refer [min-length max-length]]))

(spec/def :checkpoint/checkpoint-slug string?)
(spec/def :checkpoint/task            (spec/and string? #(min-length % 4) #(max-length % 55)))
(spec/def :checkpoint/checkpoint-id   string?)

(spec/def :checkpoint/valid (spec/keys :req-un [:checkpoint/task
                                                :checkpoint/checkpoint-id
                                                :resource/resource-url]
                                       :opt-un [:base/tags]))

(spec/def :checkpoint/query (spec/keys :req-un [:checkpoint/checkpoint-slug]))
