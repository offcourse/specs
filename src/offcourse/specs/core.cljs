(ns offcourse.specs.core
  (:require [cljs.spec.alpha :as spec]
            [offcourse.specs.helpers :refer [min-length]]
            [offcourse.specs.bookmark]
            [offcourse.specs.collection]
            [offcourse.specs.course]
            [offcourse.specs.event]
            [offcourse.specs.portrait]
            [offcourse.specs.profile]
            [offcourse.specs.error]
            [offcourse.specs.resource]
            [offcourse.specs.checkpoint]
            [offcourse.specs.status]
            [offcourse.specs.user]
            [offcourse.specs.action]))

(def url-regex #"^https?://.+\..+")
(spec/def :base/flags (spec/* string?))
(spec/def :base/tags (spec/* string?))
(spec/def :base/url (spec/and string? #(re-matches url-regex %)))
(spec/def :base/slug string?)
(spec/def :base/organization string?)
(spec/def :base/user-name (spec/and string? #(min-length % 3)))
(spec/def :base/timestamp int?)

