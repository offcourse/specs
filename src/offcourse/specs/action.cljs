(ns offcourse.specs.action
  (:require [cljs.spec.alpha :as spec]))

(defmulti action-spec (fn [[action-type _ :as action]] action-type))

(spec/def :action/valid (spec/multi-spec action-spec :action-type))

(spec/def :action/types #{:authenticate :sign-in :sign-out :sign-up :augment
                          :flag :transform :compress :deflate :update :edit
                          :remove :toggle :save :fork :reset :check :go :add
                          :trigger :switch-to :reject :ignore :create :render})
