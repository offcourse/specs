(ns offcourse.specs.course
  (:require [cljs.spec.alpha :as spec]
            [offcourse.specs.user]
            [offcourse.specs.base]
            [offcourse.specs.helpers :refer [max-length min-length]]))

(spec/def :course/curator     :user/user-name)
(spec/def :course/course-id   string?)
(spec/def :course/base-id     :course/course-id)
(spec/def :course/parent-id   (spec/nilable :course/course-id))
(spec/def :course/goal        (spec/and string? #(min-length % 4) #(max-length % 55)))
(spec/def :course/timestamp  :base/timestamp)
(spec/def :course/revision    int?)

(spec/def :course/description (spec/nilable (spec/and string? #(min-length % 10) #(max-length % 210))))

(spec/def :course/checkpoints (spec/coll-of :checkpoint/valid :min-count 1))

(spec/def :course/base (spec/keys :req-un [:course/course-id
                                           :base/repository
                                           :course/revision]))

(spec/def :course/base-id :course/course-id)
(spec/def :course/parent-id (spec/nilable :course/course-id))

(spec/def :course/valid (spec/keys :req-un [:course/course-id
                                            :course/base-id
                                            :course/parent-id
                                            :base/repository
                                            :course/curator
                                            :course/goal
                                            :course/timestamp
                                            :course/revision
                                            :course/checkpoints]
                                   :opt-un [:course/description]))

(spec/def :course/query-by-slug (spec/keys :req-un [:course/course-slug
                                                    :course/curator]))

(spec/def :course/query-by-id (spec/keys :req-un [:course/course-id]
                                         :opt-un [:course/revision]))

(spec/def :course/query       (spec/or :by-slug (spec/keys :req-un [:course/course-slug
                                                                    :course/curator])
                                       :by-id   (spec/keys :req-un [:course/course-id])))
