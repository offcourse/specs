(ns offcourse.specs.status
  (:require [cljs.spec.alpha :as spec]
            [offcourse.specs.course]
            [offcourse.specs.user]))

(spec/def :status/learner :user/user-name)
(spec/def :status/checkpoint-ids (spec/coll-of :checkpoint/checkpoint-id))

(spec/def :status/valid (spec/keys :req-un [:course/course-id
                                            :status/checkpoint-ids
                                            :status/learner]))

(spec/def :status/query (spec/keys :req-un [:course/course-id
                                            :status/learner]))

(spec/def :status/update (spec/keys :req-un [:course/course-id
                                             :checkpoint/checkpoint-id]))
