(ns offcourse.specs.error
  (:require [cljs.spec.alpha :as spec]))

(spec/def ::error-type keyword?)
(spec/def ::error any?)

(spec/def :error/error (spec/keys :req-un [::error-type ::error]))

