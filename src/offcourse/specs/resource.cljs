(ns offcourse.specs.resource
  (:require [cljs.spec.alpha :as spec]
            [offcourse.specs.base]))

(spec/def :resource/resource-url :base/url)

(spec/def :content/video-provider (spec/or :proper #{:youtube :vimeo}
                                   :api #{"youtube" "vimeo"}))
(spec/def :content/video-id string?)

(spec/def :content/html (spec/nilable string?))

(spec/def ::content        (spec/or :video (spec/keys :req-un [:content/video-provider
                                                               :content/video-id])
                                    :html (spec/keys :req-un [:content/html])))

(spec/def ::resource-type  (spec/or :proper #{:video :html}
                                    :api #{"video" "html"}))
(spec/def ::tags           (spec/coll-of string?))

(spec/def :resource/valid  (spec/keys :req-un [:resource/resource-url
                                               ::resource-type]
                                      :opt-un [::content
                                               ::tags
                                               ::description]))

(spec/def :resource/query     (spec/keys :req-un [:resource/resource-url]))
