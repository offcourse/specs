(ns offcourse.specs.bookmark
  (:require [cljs.spec.alpha :as spec]))

(spec/def :bookmark/source (spec/keys :req-un [:course/revision
                                               :course/course-id
                                               :checkpoint/checkpoint-id]))

(spec/def :bookmark/valid (spec/keys :req-un [:resource/resource-url
                                              :base/timestamp
                                              :bookmark/source]))
