(ns offcourse.specs.user
  (:require [cljs.spec.alpha :as spec]
            [offcourse.specs.helpers :refer [min-length]]))

(spec/def :user/user-name (spec/and string? #(min-length % 3)))
