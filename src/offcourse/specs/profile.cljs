(ns offcourse.specs.profile
  (:require [cljs.spec.alpha :as spec]))

(spec/def ::name string?)
(spec/def ::emails (spec/* map?))
(spec/def ::revision int?)

(spec/def :profile/valid (spec/keys :req-un [:base/user-name ::revision ::name ::emails]))
(spec/def :profile/raw (spec/keys :req-un [::profile :auth/auth-profile]))
